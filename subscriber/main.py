from functools import partial
from kfp.components import create_component_from_func
import json


# type Model struct {
# 	DomainId  int32
# 	ProjectId int32
# 	AiObject  string
# 	UserId    int32
# 	Data      string // JSON
# }

@partial(
    create_component_from_func,
    base_image="python:3.7",
    packages_to_install=["pika==1.2.0"],
)
def consume_from_MQ(
        host: str,
        username: str,
        password: str,
        queue: str
) -> str:
    import pika
    cred = pika.PlainCredentials(username=username, password=password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, credentials=cred))
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)
    method_frame, header_frame, body = channel.basic_get(queue=queue)
    res = ""
    if method_frame:
        res = body.decode()
        channel.basic_ack(method_frame.delivery_tag)
    else:
        print('No message returned')
    return res


if __name__ == '__main__':
    consume_from_MQ.component_spec.save("component.yaml")
