from functools import partial
from kfp.components import create_component_from_func
import json


# type Model struct {
# 	DomainId  int32
# 	ProjectId int32
# 	AiObject  string
# 	UserId    int32
# 	Data      string // JSON
#   {"result": "1440"}
# }

@partial(
    create_component_from_func,
    base_image="python:3.7",
    packages_to_install=["pika==1.2.0"],
)
def publish_to_MQ(
        host: str,
        username: str,
        password: str,
        exchange: str,
        body: str,
        result: str
):
    import pika
    import json
    cred = pika.PlainCredentials(username=username, password=password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, credentials=cred))
    channel = connection.channel()
    body = eval(body)
    body['data'] = result
    channel.basic_publish(exchange=exchange, routing_key="", body=json.dumps(body))
    print("Sent [x] : " + json.dumps(body))


if __name__ == '__main__':
    publish_to_MQ.component_spec.save("component.yaml")
